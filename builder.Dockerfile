FROM gitlab-registry.cern.ch/cms-cactus/ops/auto-devops/basics-cc7:tag-0.0.8
LABEL maintainer="Cactus <cactus@cern.ch>"

RUN yum -y install openssh sshpass ed && yum clean all

EXPOSE 55555

# Openshift creates a UID with no corresponding user at runtime that causes SSH to misbehave. We add a user with that UID to passwd to solve the issue.
# keep writable for openshift user group (root)
RUN adduser --system -s /bin/bash -u 123432 -g 0 server
RUN chmod 775 /etc/ssh /home 
# to help uid fix
RUN chmod 664 /etc/passwd /etc/group 

COPY StartProxyServer.sh .

USER server

ENTRYPOINT /StartProxyServer.sh

# CMD echo -e ",s/123432/`id -u`/g\\012 w" | ed -s /etc/passwd &&\
    # cat ${PASSWORD_PATH} | sshpass ssh -o StrictHostKeyChecking=no -g -tN -v -4 -D 55555 ${USERNAME}@${HOST}
  