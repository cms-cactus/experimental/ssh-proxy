#!/bin/sh

echo -e ",s/123432/`id -u`/g\\012 w" | ed -s /etc/passwd

if [ $? -eq 0 ]; then
  cat ${PASSWORD_PATH} | sshpass ssh -o StrictHostKeyChecking=no -g -tN -v -4 -D 55555 ${USERNAME}@${HOST}
else
  echo Failed to assign UID to user. Exiting.
fi